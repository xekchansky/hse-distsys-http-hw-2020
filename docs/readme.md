# Документация на мой API

Данные - key - values([value1, value2, value 3 ...]) хранилище (value - числа)

Post \data + key-values - добавить новую запись

Get \data - получить весь список key - values

Get \data\<string:key> - получить values по key

Put \data\<string:key> - заменить value по key

Patch \data\<string:key> + id, value - изменяет кусок value[id] на новый в записи по key

Delete \data\<string:key> - удаляет запись по key
