import os
import json

from flask import Flask, request

app = Flask(__name__)

# data = {key: [value1, value2 ...]
data = dict()

#to convert from json
def string_to_list(str):
    str = str.replace('[', '').replace(']', '')
    arr = list(map(int,str.split(', ')))
    return arr


@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/data', methods=['POST'])
def _data_post():
    global data
    key = request.form['key']
    values = string_to_list(request.form['values'])
    if key in data.keys():
        return 'Error: key already exists'
    data[key] = values
    return 'done'

@app.route('/data', methods=['GET'])
def _data_get():
    global data
    return json.dumps(data)

@app.route('/data/<string:key>', methods=['GET'])
def _key_get(key):
    global data
    if key not in data.keys():
        return 'Error: no such key'
    return json.dumps(data[key])

@app.route('/data/<string:key>', methods=['PUT'])
def _key_put(key):
    global data
    values = string_to_list(request.form['values'])
    if key not in data.keys():
        return 'Error: no such key'
    data[key] = values
    return 'done'

@app.route('/data/<string:key>', methods=['PATCH'])
def _key_patch(key):
    id = int(request.form['id'])
    value = int(request.form['value'])
    if key not in data.keys():
        return 'Error: no such key'
    if id > len(data[key]) or id < 0:
        return 'Error: index is out of range'
    data[key][id] = value
    return 'done'

@app.route('/data/<string:key>', methods=['DELETE'])
def _key_delete(key):
    global data
    if key not in data.keys():
        return 'Error: no such key'
    data.pop(key)
    return 'done'



app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
