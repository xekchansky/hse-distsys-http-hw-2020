import os
import json

import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

data = dict()

def post(key, values):
    values = json.dumps(values)
    msg = {'key': key, 'values': values}
    response = requests.post(URL + '/data', data=msg)
    return response

def get_all():
    response = requests.get(URL + '/data')
    return response

def get(key):
    response = requests.get(URL + '/data/'+ str(key))
    return response

def put(key, values):
    values = json.dumps(values)
    msg = {'values': values}
    response = requests.put(URL + '/data/'+ str(key), data=msg)
    return response

def patch(key, id, value):
    msg = {'id': id, 'value': value}
    response = requests.patch(URL + '/data/' + str(key), data=msg)
    return response

def delete(key):
    response = requests.delete(URL + '/data/' + str(key))
    return response

def test_data_post():

    #post 5 keys with 1 value each
    for key in range(5):
        value = key + 10
        values = list([value])
        response = post(key, values)
        data[key] = values
        check = 'done'
        assert response.text == check
        assert response.status_code == 200

    #post 1 key with 4 values
    key = 228
    values = [1, 4, 8, 8]
    response = post(key, values)
    data[key] = values
    check = 'done'
    assert response.text == check
    assert response.status_code == 200

    #post 1 key that already exists
    key = 228
    values = [4, 2, 0]
    response = post(key, values)
    check = 'Error: key already exists'
    assert response.text == check
    assert response.status_code == 200

def test_data_get():
    #check get all data
    response = get_all()
    check = json.dumps(data)
    assert response.text == check
    assert response.status_code == 200

def test_data_key_get():
    key = 228
    response = get(key)
    check = json.dumps(data[key])
    assert response.text == check
    assert response.status_code == 200

def test_data_key_put():
    key = 3
    values = [0, 0, 7]
    data[key] = values
    response = put(key, values)
    check = 'done'
    assert response.text == check
    assert response.status_code == 200
    response = get(key)
    check = json.dumps(data[key])
    assert response.text == check
    assert response.status_code == 200

def test_data_key_patch():
    key = 3
    id = 0
    value = 7
    data[key][id] = value
    response = patch(key, id, value)
    check = 'done'
    assert response.text == check
    assert response.status_code == 200

    key = 3
    id = -1
    value = 7
    response = patch(key, id, value)
    check = 'Error: index is out of range'
    assert response.text == check
    assert response.status_code == 200

    response = get(key)
    check = json.dumps(data[key])
    assert response.text == check
    assert response.status_code == 200

def test_key_delete():
    key = 228
    data.pop(key)
    response = delete(key)
    check = 'done'
    assert response.text == check
    assert response.status_code == 200

    key = 228
    response = delete(key)
    check = 'Error: no such key'
    assert response.text == check
    assert response.status_code == 200

    response = get_all()
    check = json.dumps(data)
    assert response.text == check
    assert response.status_code == 200